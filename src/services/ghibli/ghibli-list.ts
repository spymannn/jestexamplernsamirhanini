
import axios from 'axios';
const baseUrl = 'https://ghibliapi.herokuapp.com';

export const getGhiblisFilms = async (): Promise<any> =>{
    return axios.get(`${baseUrl}/films`).then((response) => {
        return response.data
    });
}

const HOUR = 60;
  
export const reformatRunningTime = (time?: number) => {
    if (!time || time === 0) {
      return '';
    }
  
    if(time > HOUR) {
        return 'more than an hour'
    } else {
        return 'less than an hour'
    }
};