import { mockGhibliListFactory } from './../../__mocks__/ghibli-list.mock';
import { getGhiblisFilms, reformatRunningTime } from './ghibli-list';

const mockGet = jest.fn();

describe('Ghibli list', () => {
    afterEach(() => {
      jest.resetAllMocks();
    });
  
    it('should call the ghibli list API and find the mock', async () => {
      // given
      const mockGhibliListList = mockGhibliListFactory();
      mockGet.mockReturnValueOnce(mockGhibliListList);
  
      // when
      const result = await getGhiblisFilms();
  
      // then
      expect(result).toEqual(mockGhibliListList);
    });

    test.each`
    time           | reformattedHours
    ${undefined}   | ${''}
    ${''}          | ${''}
    ${null}        | ${''}
    ${'90'}        | ${'more than an hour'}
    ${'50'}        | ${'less than an hour'}
  `(
    'should call the reformatRunningTime and return a valid time or empty',
    async ({time, reformattedHours}) => {
      // given
      const result = await reformatRunningTime(time);
      // then
      expect(result).toEqual(reformattedHours);
    },
  );
})