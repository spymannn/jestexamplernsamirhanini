import {render} from '@testing-library/react-native';
import React from 'react';
import CardIds from '../../component/card/Card.ids';
import { mockMovies2Factory, mockMoviesFullFactory } from '../../__mocks__/movies.mock';
import MainListContainer from './MainList';

jest.mock('react-native-webview', () => 'MockWebView');

describe('Ghibli list', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('renders correctly', () => {
    // when
    const {toJSON} = render(<MainListContainer movies={mockMoviesFullFactory()} />);

    // then
    expect(toJSON()).toMatchSnapshot();
  });

  it('renders 3 cards correctly', () => {
    // when
    const {getAllByTestId} = render(<MainListContainer movies={mockMoviesFullFactory()} />);

  
    // then
    expect(getAllByTestId(CardIds.card)).toHaveLength(3)
  });

  it('renders 2 cards correctly', () => {
    // when
    const {getAllByTestId} = render(<MainListContainer movies={mockMovies2Factory()} />);

  
    // then
    expect(getAllByTestId(CardIds.card)).toHaveLength(2)
  });

});