import React from 'react';
import {StyleSheet, View} from 'react-native';
import Card from '../../component/card/Card';

export type MainListProps = {
  movies: any[]
}


const MainListContainer = ({movies}: MainListProps) => {
  
  const getListCard = () => {
    const listView = movies.map((movie, index) => {
      return (
        <Card 
          key={index} 
          title={movie.title} 
          desc={movie.desc} 
          img={movie.img}
          onPressCard={() => console.log(`${movie.title} is ON!`)}
        />
      )
    })

    return <View>{listView}</View>
  }

  return (
    <View style={styles.container}>
      {getListCard()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
});

export default MainListContainer;
