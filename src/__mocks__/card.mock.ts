export const mockCardFactory = (): any => {
    return {
          title: `Kiki's Delivery Service`,
          desc: 'A young witch, on her mandatory year of independent life, finds fitting into a new community difficult while she supports herself by running an air courier service.',
          img: 'https://image.tmdb.org/t/p/original/h5pAEVma835u8xoE60kmLVopLct.jpg'
        }
}