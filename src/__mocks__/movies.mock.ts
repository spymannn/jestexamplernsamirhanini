export const mockMoviesFullFactory = (): any[] => {
    return [
        {
          title: `Kiki's Delivery Service`,
          desc: 'A young witch, on her mandatory year of independent life, finds fitting into a new community difficult while she supports herself by running an air courier service.',
          img: 'https://image.tmdb.org/t/p/original/h5pAEVma835u8xoE60kmLVopLct.jpg'
        },
        {
          title: 'Castle in the Sky',
          desc: 'The orphan Sheeta inherited a mysterious crystal that links her to the mythical sky-kingdom of Laputa.',
          img: 'https://image.tmdb.org/t/p/w533_and_h300_bestv2/3cyjYtLWCBE1uvWINHFsFnE8LUK.jpg'
        },
        {
          title: 'Princess Mononoke',
          desc: 'Ashitaka, a prince of the disappearing Ainu tribe, is cursed by a demonized boar god and must journey to the west to find a cure.',
          img: 'https://image.tmdb.org/t/p/original/6pTqSq0zYIWCsucJys8q5L92kUY.jpg'
        }
      ]
}

export const mockMovies2Factory = (): any[] => {
    return [
        {
          title: `Kiki's Delivery Service`,
          desc: 'A young witch, on her mandatory year of independent life, finds fitting into a new community difficult while she supports herself by running an air courier service.',
          img: 'https://image.tmdb.org/t/p/original/h5pAEVma835u8xoE60kmLVopLct.jpg'
        },
        {
          title: 'Castle in the Sky',
          desc: 'The orphan Sheeta inherited a mysterious crystal that links her to the mythical sky-kingdom of Laputa.',
          img: 'https://image.tmdb.org/t/p/w533_and_h300_bestv2/3cyjYtLWCBE1uvWINHFsFnE8LUK.jpg'
        }
      ]
}
