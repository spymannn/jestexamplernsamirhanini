import {fireEvent, render} from '@testing-library/react-native';
import React from 'react';
import { mockCardFactory } from '../../__mocks__/card.mock';
import Card from './Card';
import CardIds from './Card.ids';


const mockCard = mockCardFactory()
const mockOnPressed = jest.fn()
describe('Card', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('renders correctly', () => {
    // when
    const {toJSON} = render(
        <Card 
          title={mockCard.title} 
          desc={mockCard.desc}
          img={mockCard.img}
          onPressCard={mockOnPressed}
        />);

    // then
    expect(toJSON()).toMatchSnapshot();
  });
  

  it('should render the banner img', () => {
    // given
    const {getByTestId} = render(
      <Card 
        title={mockCard.title} 
        desc={mockCard.desc}
        img={mockCard.img}
        onPressCard={mockOnPressed}
      />);

    // then
    expect(getByTestId(CardIds.banner)).toBeDefined();
  });

  it('should render the title', () => {
    // given
    const {getByTestId} = render(
      <Card 
        title={mockCard.title} 
        desc={mockCard.desc}
        img={mockCard.img}
        onPressCard={mockOnPressed}
      />);

    // then
    expect(getByTestId(CardIds.title)).toBeDefined();
    expect(getByTestId(CardIds.title).props.children).toEqual(mockCard.title) 
    expect(getByTestId(CardIds.title).props.style).toHaveProperty('fontWeight', 'bold')
  });

  it('should render the description', () => {
    // given
    const {getByTestId} = render(
      <Card 
        title={mockCard.title} 
        desc={mockCard.desc}
        img={mockCard.img}
        onPressCard={mockOnPressed}
      />);

    // then
    expect(getByTestId(CardIds.desc)).toBeDefined();
    expect(getByTestId(CardIds.desc).props.children).toEqual(mockCard.desc)

  });

  it('should call the function passed in props', () => {
    // given
    const {getByTestId} = render(
      <Card 
        title={mockCard.title} 
        desc={mockCard.desc}
        img={mockCard.img}
        onPressCard={mockOnPressed}
      />);

    // when
    fireEvent.press(getByTestId(CardIds.button));

    // then
    expect(mockOnPressed).toHaveBeenCalledTimes(1);
    // expect(mockOnPressed).not.toHaveBeenCalledTimes(1);
  });
});