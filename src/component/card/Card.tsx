import React from 'react';
import {StyleSheet, View, Text, Image, Button} from 'react-native';
import CardIds from './Card.ids';

export type CardProps = {
  title: string;
  desc: string;
  img: string;
  onPressCard: () => void
}

const Card = ({title, desc, img, onPressCard}: CardProps) => {

  const getImg = () => {
    switch(img) {
      case 'castleinthesky':
        return require(`./../../assets/castleinthesky.jpg`);
      case 'kikiservice':
        return require(`./../../assets/kikiservice.jpg`);
      case 'princessmononoke':
        return require(`./../../assets/princessmononoke.jpg`);
    }
  }

  return (
    <View style={styles.container} testID={CardIds.card}>
       <Image
        testID={CardIds.banner}
        style = {{ width: 200 , height: 80 }}

        source={getImg()}
      />
      <Text testID={CardIds.title} style={styles.title}>{title}</Text>
      <Text testID={CardIds.desc} style={styles.desc}>
        {desc}
      </Text>
      <Button
        testID={CardIds.button}
        onPress={onPressCard}  
        title="Press"  
        color="#841584" 
        accessibilityLabel="Learn more about this purple button"/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth: 6,
    // borderColor: 'red',
    // borderStyle: 'dotted',
  },
  title: {
    fontWeight: 'bold',
    // fontWeight: '400'
  },
  desc: {
    padding: 20
  }
});

export default Card;
