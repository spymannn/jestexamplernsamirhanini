/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import MainListContainer from './src/containers/mainList/MainList';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const movies = [
    {
      title: `Kiki's Delivery Service`,
      desc: 'A young witch, on her mandatory year of independent life, finds fitting into a new community difficult while she supports herself by running an air courier service.',
      img: 'kikiservice'
    },
    {
      title: 'Castle in the Sky',
      desc: 'The orphan Sheeta inherited a mysterious crystal that links her to the mythical sky-kingdom of Laputa.',
      img: 'castleinthesky'
    },
    {
      title: 'Princess Mononoke',
      desc: 'Ashitaka, a prince of the disappearing Ainu tribe, is cursed by a demonized boar god and must journey to the west to find a cure.',
      img: 'princessmononoke'
    }
  ]

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <MainListContainer movies={movies}/>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }

});

export default App;
