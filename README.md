# React Native Application + JEST Example

React-Native Application to check Jest test example 

## Lancer l'application / Launch the application

- Installez les dépendances / Install all the dependencies (`npm ci`).
- Lancez un émulateur android/ios. Launch an android/ios emulator
- Roulez la commande `npm run <ios | android>` pour installer et lancer l'application sur l'émulateur. Run the commande `npm run <ios | android>` to install and launch the application on the emulators
- Rouler les tests / Run all the tests (`npm run test`).
- Rouler les tests en direct / Run the test in watch mode (`npm run test:watchAll`)

## Tests Files
- > src/services/ghibli-list.test.ts
- > src/mainList/MainList.test.tsx
- > src/component/card/Card.test.tsx


## Questions
If any questions, ask @SamirHanini (samir.hanini@alithya.com)
